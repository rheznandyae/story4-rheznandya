from django.shortcuts import render
from django.http import JsonResponse
import requests

# Create your views here.

def story8_views(request, *args, **kwargs):
    response = {
        
    }
    return render(request, "story8.html", response)

def json_response(request, *args, **kwargs):
    resp = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + kwargs["searchKey"])

    bookjson = resp.json()

    response = {
        "daftarBuku": [],
    }
    bookjson = bookjson["items"]
    
    for dataBuku in bookjson:
        buku = dataBuku["volumeInfo"]
        jsonBuku = {}


        jsonBuku["foto"] = buku["imageLinks"]["thumbnail"] if "imageLinks" in buku.keys() else "https://images.unsplash.com/photo-1484800089236-7ae8f5dffc8e?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1868&q=80"
        jsonBuku["judul"] = buku["title"] if "title" in buku.keys() else "Not Found"
        jsonBuku["deskripsi"] = buku["description"] if "description" in buku.keys() else "Not Found"

        response["daftarBuku"].append(jsonBuku)

    respon = JsonResponse(response)

    return respon

