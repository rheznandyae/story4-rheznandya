from django.test import TestCase

class Test_Story8(TestCase):
    def test_url(self):
        response = self.client.get("/story8/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story8.html')

    def test_searchBox(self):
        response = self.client.get("/story8/json/tan malaka")
        self.assertEqual(response.status_code, 200)
