from django.urls import path
from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.story8_views, name='home-story8'),
    path('json/<str:searchKey>', views.json_response, name='json-story8-search'),
]