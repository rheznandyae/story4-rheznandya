from django.shortcuts import render
from django.http import HttpResponse 


def main_content(request, *args, **kwargs):
    return render(request, "story3/main.html",{})

def gallery_content(request, *args, **kwargs):
    return render(request, "story3/gallery.html",{})


