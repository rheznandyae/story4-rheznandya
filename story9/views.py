from django.shortcuts import render, redirect, reverse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout


# Create your views here.

def story9_view(request, *args, **kwargs):
    user = request.user.username
    response = {
        "username" : user
    }
    return render(request, "story9.html", response)

def login_view(request, *args, **kwargs):
    response = {"messages" : ""} 
    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")
        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect("/story9/")
        else:
            response["messages"] = "Credentials not valid"
            return render(request, "login.html", response)
    
    return render(request, "login.html", response)

def register_view(request, *args, **kwargs):

    response = {"messages" : ""} 

    if request.method == "POST":
        email = request.POST.get("email")
        username = request.POST.get("username")
        password = request.POST.get("password")
        confirmedPass = request.POST.get("confirmedPassword")

        if password != confirmedPass :
            response["messages"] = "confirmed password not same"
            return render(request, "register.html", response)
        elif email == "" or username == "" or password == "" or confirmedPass == "" :
            response["messages"] = "Please fill the blank"
            return render(request, "register.html", response)
        elif User.objects.filter(username=username).exists() :
            response["messages"] = "Username already exist"
            return render(request, "register.html", response)
        else :
            user = User.objects.create_user(username, email , confirmedPass)
            return redirect("/story9/login")
    return render(request, "register.html", response)    

def logout_view(request):
    logout(request)
    return redirect("/story9/")
