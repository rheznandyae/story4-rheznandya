from django.test import TestCase

class Test_Story9(TestCase):
    def test_url(self):
        response = self.client.get("/story9/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9.html')

    def test_register_page(self):
        response = self.client.get("/story9/register/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'register.html')


        self.client.post('/story9/register/', data={
            "email" : "blarrrrrr.gmail.com",
            "username" : "halahhh",
            "password": "1234",
            "confirmedPassword": "1234",
        })
        response = self.client.get("/story9/login/")
        self.assertEqual(response.status_code, 200)

        response = self.client.post('/story9/register/', data={
            "email" : "blabla.gmail.com",
            "username" : "testUser",
            "password": "1234",
            "confirmedPassword": "2222",
        })
        self.assertEqual(response.status_code, 200)

        response = self.client.post('/story9/register/', data={
            "email" : "",
            "username" : "",
            "password": "",
            "confirmedPassword": "",
        })
        self.assertEqual(response.status_code, 200)

        response = self.client.post('/story9/register/', data={
            "email" : "adasd@gmail.com",
            "username" : "halahhh",
            "password": "aaaaa",
            "confirmedPassword": "aaaaa",
        })
        self.assertEqual(response.status_code, 200)


    def test_login_page(self):
        response = self.client.get("/story9/login/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html') 
        
        self.client.post('/story9/register/', data={
            "email" : "blabla.gmail.com",
            "username" : "testUser",
            "password": "1234",
            "confirmedPassword": "1234",
        })

        response = self.client.post("/story9/login/", data={
            "username" : "hiya",
            "password" : "1111",
        })
        self.assertEqual(response.status_code, 200)

        self.client.post("/story9/login/", data={
            "username" : "testUser",
            "password" : "1234",
        })
        response = self.client.get("/story9/")
        self.assertEqual(response.status_code, 200)

    def test_logout(self):
        self.client.post('/story9/register/', data={
            "email" : "blabla.gmail.com",
            "username" : "testUser",
            "password": "1234",
            "confirmedPassword": "1234",
        })
        
        self.client.post("/story9/login/", data={
            "username" : "testUser",
            "password" : "1234",
        })

        self.client.get("/story9/logout/")
        responseLogout = self.client.get("/story9/")
        self.assertEqual(responseLogout.status_code, 200)
        