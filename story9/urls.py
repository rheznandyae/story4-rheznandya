from django.urls import path
from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.story9_view, name='story9'),
    path('login/', views.login_view, name='login'),
    path('register/', views.register_view, name='register'),
    path('logout/', views.logout_view, name='logout'),
]