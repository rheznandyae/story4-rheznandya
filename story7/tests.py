from django.test import TestCase

class Test_Story7(TestCase):
    def test_url(self):
        response = self.client.get("/story7/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story7.html')
